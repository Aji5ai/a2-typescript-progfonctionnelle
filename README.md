# Contexte du projet
_Ce projet est fictif et réalisé dans le cadre d'un cours_

SimplonDelivery, une entreprise spécialisée en logistique internationale, nous confie le développement d'un prototype d'application. Cette application, destinée à la gestion des conversions de devises et au calcul des frais de livraison, sera un outil clé dans l'optimisation de leurs services. L'objectif est de créer ce prototype en utilisant TypeScript, sans utiliser de programmation orientée objet.

# Initialisation du projet
```
npm init -y
npm i --save-dev @types/node
npm install typescript --save-dev 
npx tsc --init 
```
Faire un fichier index.ts dans un dossier src

Mettre ce qui suit dans tsconfig (en remplacement de ce qui y était déjà):
```
{
  "compilerOptions": {
    "module": "nodenext",
    "moduleResolution": "nodenext",
    "target": "es2022",
    "strict": true,
    "esModuleInterop": true,
    "forceConsistentCasingInFileNames": true,
    "outDir": "./dist"
  },

  "include": ["src/**/*"],
  "exclude": ["**/*.spec.ts"]
}
```

Puis au choix pour exécuter : 
```
npx tsc (ou npx tsc --watch en mode serveur)
node dist/index.js
```

Sinon plus simple :

Installer ts node :
```
npm install -g ts-node
```

Puis pour transpiler ET exécuter en une ligne :
```
npx ts-node src/index.ts
```

<!-- blank line -->
----
<!-- blank line -->

# Explication du code
Le code comporte 4 fonctions.

3 fonctions permettant de :
- convertir des devises (**exchange()**), 
- de calculer des frais de livraison (**delivery()**) 
- et de calculer des frais de douane (**customs()**).

La fonction **main()** permet de les appeler selon le choix de l'utilisateur.

La fonction **exchange()** est appelée deux fois, soit par main qui doit alors poser des questions à l'utilisateur avant de passer les input à **exchange()**, soit par **customs()** qui doit faire pareil (tout en complétant avec ses variables).

<!-- blank line -->
----
<!-- blank line -->

# Exemples d'utilisation

## Pour convertir 5 euros en dollars canadiens :
Exécuter ```npx ts-node src/index.ts``` dans le terminal puis répondre aux questions comme suit :
 - Taper '1' puis la touche 'entrée' pour accéder au convertisseur de devises
 - Le programme demande "Quelle est votre devise de départ ? (eur, cad ou jpy) :" 
    - tapez donc 'eur' puis la touche 'entrée'
 - Nouvelle question : "Dans quelle devise voulez-vous convertir ? (eur, cad ou jpy) :" 
    - tapez donc 'cad' puis la touche 'entrée'
 - À la question "Et pour quel montant ? :"
    - tapez "5" 
 - Le programme vous retourne alors la phrase : "Voici le montant obtenu : 8 cad" et mets fin au readline. 

Vous pouvez re exécuter le programme en tapant ```npx ts-node src/index.ts``` ou bien ```node dist/index.js```

## Pour calculer les frais de livraison d'un colis de 5 kilos, de longueur 20cm, largeur 30 cm et hauteur 50 cm à destination du japon :
Exécuter ```npx ts-node src/index.ts``` dans le terminal puis répondre aux questions comme suit :
 - Taper '2' puis la touche 'entrée' pour accéder au calculateur des frais de livraison
 - Le programme demande "Séparés par des virgules et dans cet ordre, entrez les informations de votre colis:
 poids, longueur, largeur, hauteur, pays de destination (france, canada ou japon):"
    - Il faut donc taper : 5,20,30,50,japon
 Attention, pas d'espaces !
 - Le programme retourne alors : "Frais de livraison : 3000 yens" et se termine. 
 
Vous pouvez re exécuter le programme en tapant ```npx ts-node src/index.ts``` ou bien ```node dist/index.js```

## Pour calculer les frais de douane d'un colis d'une valeur de 200 euros à destination du canada :
Exécuter ```npx ts-node src/index.ts``` dans le terminal puis répondre aux questions comme suit :
 - Taper '3' puis la touche 'entrée' pour accéder au calculateur des frais de douane
 - Le programme demande "Quelle est la valeur de votre colis en euros :" 
    - tapez donc '200' puis la touche 'entrée'
 - "Où désirez-vous envoyer votre colis ? (france, japon ou canada):"
    - répondez 'canada' à cette question 
 - Le programme répond "Vous devrez payer une taxe d'un montant de : 45 dollars canadiens" et se termine. 

Vous pouvez re exécuter le programme en tapant ```npx ts-node src/index.ts``` ou bien ```node dist/index.js```


