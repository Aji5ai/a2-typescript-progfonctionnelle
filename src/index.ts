// readline permet de prompt l'utilisateur avec node
const readline = require("readline"); // faire npm i --save-dev @types/node si marche pas

// crée une interface de lecture (readline.Interface) dans Node.js en utilisant le module readline
let rl = readline.createInterface(process.stdin, process.stdout);

// Fonction principale qui appelle les trois autres
/**
 * main demande à l'utilisateur s'il veut convertir une somme, calculer le prix de l'envoi d'un colis, ou calculer les frais de douane.
 * @param aucun tout se fait avec des readline
 * @return rien car ce sont des console log
 */
function main(): void {
  rl.question(
    "Pour convertir des devises, tapez '1'. Pour calculer vos frais de livraison, tapez '2'. Pour calculer vos frais de douanes, tapez '3'. \n Que désirez-vous faire ? ",
    (answer: string) => {
      // Si la réponse est 1 on doit poser des questions car la fonction exchange est également appelée dans la fonction customs, et par conséquent on doit pouvoir lui passer des paramètres (obtenus soit par les questiosn dans main, soit par passage d'arguments dans customs) et elle doit retourner un nombre.
      if (answer === "1") {
        rl.question(
          "Quelle est votre devise de départ ? (eur, cad ou jpy) : ",
          (currency: string) => {
            if (
              // vérification, si no eur, ni cad, ni jpy ne sont marqués dans currency, on ferme readline
              !(
                currency.includes("eur") ||
                currency.includes("cad") ||
                currency.includes("jpy")
              )
            ) {
              console.log(
                "Vous n'avez pas entré une devise sous le bon format"
              );
              rl.close();
            } else {
              // si les currency sont bien renseignées on continue
              rl.question(
                "Dans quelle devise voulez-vous convertir ? (eur, cad ou jpy) : ",
                (exchangeCurrency: string) => {
                  if (exchangeCurrency == currency) {
                    console.log(
                      "Vous devez choisir une devise différente pour convertir."
                    );
                    rl.close();
                  } else {
                    rl.question(
                      "Et pour quel montant ? : ",
                      (inputAmount: number) => {
                        // Appel de exchange avec les arguments obtenus des input utilisateur
                        console.log(
                          `Voici le montant obtenu :`,
                          exchange(
                            currency,
                            exchangeCurrency,
                            inputAmount
                          ).toString(),
                          exchangeCurrency
                        );
                      }
                    );
                  }
                }
              );
            }
          }
        );
      } else if (answer === "2") {
        delivery();
      } else if (answer === "3") {
        customs();
      } else {
        console.log("Veuillez entrer 1, 2 ou 3.");
        rl.close();
      }
    }
  );
}

main();

//1. Convertisseur de devises
/**
 * exchange permet de convertir une somme dans une autre devise selon des taux de conversion pré établis.
 * @param currency1 eur, cad ou jpy
 * @param currency2 eur, cad ou jpy
 * @param amount le montant à convertir
 * @returns number - le montant converti
 */
function exchange(
  currency1: string,
  currency2: string,
  amount: number
): number {
  // Les taux de conversions
  const euroToCadOrJpy: number[] = [1.5, 130];
  const cadToEurOrJpy: number[] = [0.67, 87];
  const JpyToEurOrCad: number[] = [0.0077, 0.0115];

  // initialisation du montant une fois converti
  let convertedAmount: number = 0;
  switch (currency1) {
    case "eur":
      switch (currency2) {
        case "cad":
          // arrondi à l'entier le plus proche le montant converti
          convertedAmount = Math.round(amount * euroToCadOrJpy[0]);
          break;
        case "jpy":
          convertedAmount = Math.round(amount * euroToCadOrJpy[1]);
          break;
      }
      break;
    case "cad":
      switch (currency2) {
        case "eur":
          convertedAmount = Math.round(amount * cadToEurOrJpy[0]);
          break;
        case "jpy":
          convertedAmount = Math.round(amount * cadToEurOrJpy[1]);
          break;
      }
      break;
    case "jpy":
      switch (currency2) {
        case "eur":
          convertedAmount = Math.round(amount * JpyToEurOrCad[0]);
          break;
        case "cad":
          convertedAmount = Math.round(amount * JpyToEurOrCad[1]);
          break;
      }
      break;
  }
  rl.close();
  return convertedAmount; // retourne le nombre pour pouvoir être utilisé par d'autres fonctions
}

// 2. Calculateur de frais de livraison
function delivery(): void {
  rl.question(
    "Séparés par des virgules et dans cet ordre, entrez les informations de votre colis: \n poids, longueur, largeur, hauteur, pays de destination (france, canada ou japon): ",
    (input: string) => {
      // on prend tout en string pour pouvoir ensuite les séparer avec les virgules
      const [weightStr, lengthStr, widthStr, heightStr, destinationStr] =
        input.split(","); // le split ne marche que sur un string

      // Convertir les nombres de string à number
      const weight: number = parseInt(weightStr, 10);
      const length: number = parseInt(lengthStr, 10);
      const width: number = parseInt(widthStr, 10);
      const height: number = parseInt(heightStr, 10);

      // Convertir le pays en minuscules
      const destination: string = destinationStr.toLowerCase();

      // Gestion de quelques mauvais input :
      if (weight == 0 || length > 200 || width > 200 || height > 200) {
        console.log(
          "Veuillez vérifier les informations entrées. Le poids et les dimensions du colis doivent être positif. Les dimensions ne doivent pas dépasser 2 mètres."
        );
        rl.close();
      } else if (
        !(
          destination.includes("france") ||
          destination.includes("canada") ||
          destination.includes("japon")
        )
      ) {
        console.log("Vous devez taper france, japon ou canada en destination");
        rl.close();
      } else {
        console.log(
          `Vérification de ce que vous avez entré : \n weight: ${weight}, length: ${length}, width: ${width}, height: ${height}, destination: ${destination}`
        );

        let cost: number = 0;

        // Ajout d'un supplément si les dimensions dépassent 1.5m en total
        if (length + width + height > 150) {
          switch (destination) {
            case "france":
              cost = 5;
              break;
            case "canada":
              cost = 7.5;
              break;
            case "japon":
              cost = 500;
              break;
          }
        }
        // Puis vérification du poids pour ajouter le prix
        if (weight < 1) {
          switch (destination) {
            case "france":
              cost += 10;
              console.log(`Frais de livraison : ${cost} euros`);
              break;
            case "canada":
              cost += 15;
              console.log(`Frais de livraison : ${cost} dollars canadiens`);
              break;
            case "japon":
              cost += 1000;
              console.log(`Frais de livraison : ${cost} yens`);
              break;
          }
        } else if (weight <= 3) {
          switch (destination) {
            case "france":
              cost += 20;
              console.log(`Frais de livraison : ${cost} euros`);
              break;
            case "canada":
              cost += 30;
              console.log(`Frais de livraison : ${cost} dollars canadiens`);
              break;
            case "japon":
              cost += 2000;
              console.log(`Frais de livraison : ${cost} yens`);
              break;
          }
        } else if (weight > 3) {
          switch (destination) {
            case "france":
              cost += 30;
              console.log(`Frais de livraison : ${cost} euros`);
              break;
            case "canada":
              cost += 45;
              console.log(`Frais de livraison : ${cost} dollars canadiens`);
              break;
            case "japon":
              cost += 3000;
              console.log(`Frais de livraison : ${cost} yens`);
              break;
          }
        }
        rl.close();
      }
    }
  );
}

// 3. Calcul de frais de douanes
function customs(): void {
  rl.question(
    "Quelle est la valeur de votre colis en euros : ",
    (value: number) => {
      rl.question(
        "Où désirez-vous envoyer votre colis ? (france, japon ou canada): ",
        (country: string) => {
          let withTax: number;
          country = country.toLowerCase();

          // convertir value dans la devise du pays de destination
          let currencyOfDestination: string = "";
          if (country == "canada") {
            currencyOfDestination = "cad";
          } else if (country == "japon") {
            currencyOfDestination = "jpy";
          } else if (country == "france") {
            currencyOfDestination = "jpy";
          } else {
            console.log(
              "Veuillez renseigner un des trois pays suivants : france, japon ou canada."
            );
            rl.close();
          }

          // On converti avec exchange la valeur du colis
          const valueConverted: number = exchange(
            "eur",
            currencyOfDestination,
            value
          );

          if (valueConverted > 20 && country === "canada") {
            withTax = valueConverted + valueConverted * 0.15;
            console.log(
              `Vous devrez payer une taxe d'un montant de : ${
                valueConverted * 0.15
              } dollars canadiens`
            );
          } else if (valueConverted > 5000 && country === "japon") {
            withTax = valueConverted + valueConverted * 0.1;
            console.log(
              `Vous devrez payer une taxe d'un montant de : ${
                valueConverted * 0.1
              } yens`
            );
          } else {
            console.log("Vous n'avez pas de frais de douane supplémentaire.");
          }
          rl.close();
        }
      );
    }
  );
}
